package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
       
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    String computerMove;
    String playerMove;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    
    
    
    public static String computerMoveInput()
    {
        String computerMove;
        Random random = new Random();
        int cInput = random.nextInt(3)+1;
        if (cInput == 1)
            computerMove = "rock";
        else if(cInput == 2)
            computerMove = "paper";
        else
            computerMove = "scissors";
        
        return computerMove;
    }
    
    public void run() {
        //  Implement Rock Paper Scissors
    while(true) {
    	System.out.println("Let's play round " + roundCounter);
    	
		while (true) {
    	playerMove = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
    	
    	if(playerMove.equals("scissors")) {
    		break;
    	} else if(playerMove.equals("paper")) {
    		break;
    	} else if(playerMove.equals("rock")) {
    		break;
    	}else {
    	System.out.println("I do not understand " + playerMove + ". Could you try again?");
    	continue;
    	}
    	}
    	
    	String computerMove = computerMoveInput();
	
    	String result;
    	
		if(playerMove.equals("rock") && computerMove.equals("scissors")) {
		humanScore += 1;
		result = "Player Wins!";
		
	}	else if(playerMove.equals("scissors") && computerMove.equals("paper")){
		humanScore += 1;
		result = "Player Wins!";
		
	}	else if(playerMove.equals("paper") && computerMove.equals("rock")){
		humanScore += 1;
		result = "Player Wins!";

	}	else if(playerMove.equals("rock") && computerMove.equals("paper")){
		computerScore += 1;
		result = "Computer Wins!";
		
	}	else if(playerMove.equals("scissors") && computerMove.equals("rock")){
		computerScore += 1;
		result = "Computer Wins!";
		
	}	else if(playerMove.equals("paper") && computerMove.equals("scissors")){
		computerScore += 1;
		result = "Computer Wins!";
		
	} 	else {
		result = "It's a tie!";
	}
		System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". " + result);
    	
		System.out.println("Score: human " + humanScore +", computer " + computerScore);
    	
		String contPlay = readInput("Do you wish to continue playing? (y/n)?");
    	
    	if(contPlay.equals("y")) {
    		roundCounter += 1;
    		continue;
    	
    	} else if(contPlay.equals("n")) {
    		System.out.println("Bye bye :)");
    		break;
    	}}
    	
    }


	/**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
